package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return entities.stream().filter(user -> login.equals(user.getLogin())).findFirst().orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return entities.stream().filter(user -> email.equals(user.getEmail())).findFirst().orElse(null);
    }

    @Override
    public void removeByLogin(final String login) {
        final User user = findByLogin(login);
        entities.remove(user);
    }

}
