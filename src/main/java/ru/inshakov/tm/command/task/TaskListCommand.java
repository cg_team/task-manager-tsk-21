package ru.inshakov.tm.command.task;

import ru.inshakov.tm.api.service.IProjectService;
import ru.inshakov.tm.api.service.ITaskService;
import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.enumerated.Sort;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "show all tasks";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        final ITaskService taskService = IServiceLocator.getTaskService();
        if (!Optional.ofNullable(sort).isPresent()) tasks = taskService.findAll(userId);
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(userId, sortType.getComparator());
        }
        tasks.forEach(System.out::println);
    }

}
