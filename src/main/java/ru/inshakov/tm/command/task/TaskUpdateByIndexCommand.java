package ru.inshakov.tm.command.task;

import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "update task by index";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = IServiceLocator.getTaskService().findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = IServiceLocator.getTaskService().updateByIndex(userId, index, name, description);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
