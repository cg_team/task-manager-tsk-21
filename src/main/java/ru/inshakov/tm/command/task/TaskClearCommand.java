package ru.inshakov.tm.command.task;

import ru.inshakov.tm.command.AbstractTaskCommand;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "clear all tasks";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[TASK CLEAR]");
        IServiceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
    }

}