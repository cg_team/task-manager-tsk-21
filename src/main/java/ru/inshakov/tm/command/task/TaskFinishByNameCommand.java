package ru.inshakov.tm.command.task;

import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String description() {
        return "finish task by name";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = IServiceLocator.getTaskService().finishByName(userId, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
