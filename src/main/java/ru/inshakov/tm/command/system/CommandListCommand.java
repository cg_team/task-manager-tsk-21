package ru.inshakov.tm.command.system;

import ru.inshakov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class CommandListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "command-list";
    }

    @Override
    public String description() {
        return "Show program commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = IServiceLocator.getCommandService().getCommands();
        commands.stream().filter(command -> Optional.ofNullable(command.name()).isPresent()).forEach((command -> System.out.println(command.name())));
    }

}
