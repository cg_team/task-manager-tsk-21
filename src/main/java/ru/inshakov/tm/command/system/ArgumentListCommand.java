package ru.inshakov.tm.command.system;

import ru.inshakov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class ArgumentListCommand extends AbstractCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "arg-list";
    }

    @Override
    public String description() {
        return "Show program arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = IServiceLocator.getCommandService().getCommands();
        commands.stream().filter(command -> Optional.ofNullable(command.arg()).isPresent()).forEach((command -> System.out.println(command.arg())));
    }
}
