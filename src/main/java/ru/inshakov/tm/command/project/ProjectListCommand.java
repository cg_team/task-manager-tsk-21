package ru.inshakov.tm.command.project;

import ru.inshakov.tm.api.service.IProjectService;
import ru.inshakov.tm.command.AbstractProjectCommand;
import ru.inshakov.tm.enumerated.Sort;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "show project list";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects;
        final IProjectService projectService = IServiceLocator.getProjectService();
        if (!Optional.ofNullable(sort).isPresent()) projects = projectService.findAll(userId);
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(userId, sortType.getComparator());
        }
        projects.forEach(System.out::println);

    }

}
