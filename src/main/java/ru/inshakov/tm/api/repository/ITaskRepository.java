package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> removeAllTasksByProjectId(String userId, String projectId);
}
