package ru.inshakov.tm.api.service;

import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.model.Project;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(String userId, String projectId);

    Task bindTaskByProjectId(String userId, String projectId, String taskId);

    Task unbindTaskByProjectId(String userId, String projectId, String taskId);

    Project removeProjectById(String userId, String projectId);

}
