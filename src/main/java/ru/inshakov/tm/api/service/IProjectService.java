package ru.inshakov.tm.api.service;

import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IBusinessService<Project> {

    Project add(String userId, String name, String description);

}
