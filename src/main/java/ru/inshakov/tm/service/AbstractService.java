package ru.inshakov.tm.service;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.entity.EntityNotFoundException;
import ru.inshakov.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(final E entity) {
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        repository.add(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E findById(String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return repository.findById(id);
    }

    @Override
    public void remove(final E entity) {
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        repository.remove(entity);
    }

    @Override
    public E removeById(String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return repository.removeById(id);
    }

}


