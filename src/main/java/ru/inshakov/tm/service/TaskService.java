package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.api.service.ITaskService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.exception.system.UndefinedErrorException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.Task;

import ru.inshakov.tm.util.ValidationUtil;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static ru.inshakov.tm.util.ValidationUtil.isEmpty;

public final class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

}
